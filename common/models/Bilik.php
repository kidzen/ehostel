<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "bilik".
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property string $no_bilik
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Hostel $hostel
 * @property StudentBilik[] $studentBiliks
 */
class Bilik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bilik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_bilik', ], 'required'],
//            [['hostel_id', 'no_bilik', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['hostel_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_bilik'], 'string', 'max' => 255],
            [['no_bilik'], 'unique'],
            [['hostel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hostel::className(), 'targetAttribute' => ['hostel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hostel_id' => 'Hostel ID',
            'no_bilik' => 'No Bilik',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostel()
    {
        return $this->hasOne(Hostel::className(), ['id' => 'hostel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentBiliks()
    {
        return $this->hasMany(StudentBilik::className(), ['bilik_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BilikQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BilikQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }
}
