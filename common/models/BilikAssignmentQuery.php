<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BilikAssignment]].
 *
 * @see BilikAssignment
 */
class BilikAssignmentQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    public function inactive()
    {
        return $this->andWhere('[[status]]=0');
    }

    /**
     * @inheritdoc
     * @return BilikAssignment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BilikAssignment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
