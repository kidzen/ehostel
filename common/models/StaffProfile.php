<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "staff".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $no_staff
 * @property string $no_ic
 * @property string $no_tel
 * @property string $email
 * @property string $nama_staff
 * @property integer $dept_id
 * @property string $gambar
 * @property integer $staff_employer_id
 * @property integer $position_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Position $position
 * @property User $user
 */
class StaffProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_staff', 'no_ic', 'email', 'nama_staff', 'dept_id', 'staff_employer_id', 'position_id'], 'required'],
//            [['user_id', 'no_staff', 'no_ic', 'no_tel', 'email', 'nama_staff', 'dept_id', 'staff_employer_id', 'position_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['user_id', 'dept_id', 'staff_employer_id', 'position_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_staff', 'no_ic', 'no_tel', 'email', 'nama_staff', 'gambar'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['no_ic'], 'unique'],
            [['no_tel'], 'unique'],
            [['email'], 'unique'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'no_staff' => 'No Staff',
            'no_ic' => 'No Ic',
            'no_tel' => 'No Tel',
            'email' => 'Email',
            'nama_staff' => 'Nama Staff',
            'dept_id' => 'Dept ID',
            'gambar' => 'Gambar',
            'staff_employer_id' => 'Staff Employer ID',
            'position_id' => 'Position ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return StaffProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StaffProfileQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }
}
