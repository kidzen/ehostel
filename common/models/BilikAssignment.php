<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "student_bilik".
 *
 * @property integer $id
 * @property integer $student_id
 * @property integer $bilik_id
 * @property string $date_register
 * @property string $valid_until
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Bilik $bilik
 * @property Student $student
 */
class BilikAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_bilik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'bilik_id', ], 'required'],
//            [['student_id', 'bilik_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['student_id', 'bilik_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['date_register', 'valid_until', 'created_at', 'updated_at'], 'safe'],
            [['bilik_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bilik::className(), 'targetAttribute' => ['bilik_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentProfile::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student ID',
            'bilik_id' => 'Bilik ID',
            'date_register' => 'Date Register',
            'valid_until' => 'Valid Until',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBilik()
    {
        return $this->hasOne(Bilik::className(), ['id' => 'bilik_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * @inheritdoc
     * @return BilikAssignmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BilikAssignmentQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }
}
