<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Bilik]].
 *
 * @see Bilik
 */
class BilikQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    public function inactive()
    {
        return $this->andWhere('[[status]]=0');
    }

    /**
     * @inheritdoc
     * @return Bilik[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bilik|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
