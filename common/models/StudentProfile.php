<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $nama
 * @property string $no_matrik
 * @property string $no_ic
 * @property string $no_tel
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property StudentBilik[] $studentBiliks
 */
class StudentProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'no_matrik', 'no_ic', ], 'required'],
//            [['nama', 'no_matrik', 'no_ic', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama', 'no_matrik', 'no_ic', 'no_tel', 'email'], 'string', 'max' => 255],
            [['no_matrik'], 'unique'],
            [['no_ic'], 'unique'],
            [['no_tel'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'no_matrik' => 'No Matrik',
            'no_ic' => 'No Ic',
            'no_tel' => 'No Tel',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentBiliks()
    {
        return $this->hasMany(StudentBilik::className(), ['student_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return StudentProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StudentProfileQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }
}
