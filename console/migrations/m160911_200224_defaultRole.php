<?php

use yii\db\Migration;

class m160911_200224_defaultRole extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->insert('{{%role}}', ['id' => 1, 'name' => 'Administrator', 'description' => 'This role can control all', 'created_at' => new yii\db\Expression('UNIX_TIMESTAMP()'), 'updated_at' => new yii\db\Expression('UNIX_TIMESTAMP()'),]);
        $this->insert('{{%role}}', ['id' => 2, 'name' => 'Staff', 'description' => 'This role can alter and view data', 'created_at' => new yii\db\Expression('UNIX_TIMESTAMP()'), 'updated_at' => new yii\db\Expression('UNIX_TIMESTAMP()'),]);
        $this->insert('{{%role}}', ['id' => 3, 'name' => 'Student', 'description' => 'This role can only view data', 'created_at' => new yii\db\Expression('UNIX_TIMESTAMP()'), 'updated_at' => new yii\db\Expression('UNIX_TIMESTAMP()'),]);

        $this->insert('{{%department}}', [
            'id' => 1,
            'name' => 'IT',
            'hod_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);

        $this->insert('{{%position}}', [
            'id' => 1,
            'position' => 'General Manager',
            'department_id' => 1,
            'leave_assignment_count' => 18,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);
    }

    public function addUser() {

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'Admin1',
            'role_id' => 1,
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
            'email' => 'admin1@mail.com',
            'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
//            'last_log' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->insert('{{%staff}}', [
            'id' => 1,
            'user_id' => 1,
            'no_staff' => 'staff123',
            'no_ic' => '901231-01-1234',
            'no_tel' => '012-34567890',
            'email' => 'admin1@mail.com',
            'nama_staff' => 'Nama Staff 1',
            'dept_id' => 1,
            'staff_employer_id' => 1,
            'position_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);
    }

    public function down() {
//        $this->truncateTable('{{%role}}');
//        $this->delete('{{%role}}', ['id' => 1]);
//        $this->delete('{{%role}}', ['id' => 2]);
//        $this->delete('{{%role}}', ['id' => 3]);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
