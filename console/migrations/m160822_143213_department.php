<?php

use yii\db\Migration;

class m160822_143213_department extends Migration
{
    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'hod_id' => $this->integer(),
            
            
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);
        
        $this->addForeignKey('fk_position_department_department_id', '{{%position}}', 'department_id', '{{%department}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_position_department_department_id', '{{%position}}');
        $this->dropTable('{{%department}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
