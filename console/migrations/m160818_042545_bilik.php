<?php

use yii\db\Migration;

class m160818_042545_bilik extends Migration {

    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bilik}}', [
            'id' => $this->primaryKey(),
            'hostel_id' => $this->integer()->notNull(),
            'no_bilik' => $this->string()->notNull()->unique(),
            
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_bilik_hostel_id', '{{%bilik}}', 'hostel_id', '{{%hostel}}', 'id', 'cascade', 'cascade');
    }

    public function down() {
        $this->dropForeignKey('fk_bilik_hostel_id', '{{%bilik}}');
        $this->dropTable('{{%bilik}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
