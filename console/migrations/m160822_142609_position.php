<?php

use yii\db\Migration;

class m160822_142609_position extends Migration
{
    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%position}}', [
            'id' => $this->primaryKey(),
            'position' => $this->string()->notNull(),
            'department_id' => $this->integer()->notNull(),
            'leave_assignment_count' => $this->integer()->notNull(),
            
            
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
                ], $tableOptions);
        
        $this->addForeignKey('fk_staff_position_position_id', '{{%staff}}', 'position_id', '{{%position}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_staff_position_position_id', '{{%staff}}');
        $this->dropTable('{{%position}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
