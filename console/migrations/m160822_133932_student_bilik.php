<?php

use yii\db\Migration;

class m160822_133932_student_bilik extends Migration
{
    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%student_bilik}}', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer()->notNull(),   //staff or student id
            'bilik_id' => $this->integer()->notNull(),   //staff or student id
            'date_register' => $this->date(),
            'valid_until' => $this->date(),
            
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_student_bilik_student_id', '{{%student_bilik}}', 'student_id', '{{%student}}', 'id');
        $this->addForeignKey('fk_student_bilik_bilik_id', '{{%student_bilik}}', 'bilik_id', '{{%bilik}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_student_bilik_student_id', '{{%student_bilik}}');
        $this->dropForeignKey('fk_student_bilik_bilik_id', '{{%student_bilik}}');
        $this->dropTable('{{%student_bilik}}');
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
