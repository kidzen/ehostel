<?php

use yii\db\Migration;

class m160822_141451_role extends Migration {

    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_user_role_id', '{{%user}}', 'role_id', '{{%role}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_user_role_id', '{{%user}}');
        $this->dropTable('{{%role}}');
    }

}
