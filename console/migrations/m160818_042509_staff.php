<?php

use yii\db\Migration;

class m160818_042509_staff extends Migration {
    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%staff}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'no_staff' => $this->string()->notNull(),
            'no_ic' => $this->string()->notNull()->unique(),
            'no_tel' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'nama_staff' => $this->string()->notNull(),
            'dept_id' => $this->integer()->notNull(),
            'gambar' => $this->string()->notNull()->defaultValue('tiada_gambar.jpg'),
            'staff_employer_id' => $this->integer()->notNull(),
            'position_id' => $this->integer()->notNull(),
            
            
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
                ], $tableOptions);
        
        $this->addForeignKey('fk_staff_user_user_id', '{{%staff}}', 'user_id', '{{%user}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_staff_user_user_id', '{{%staff}}');
        $this->dropTable('{{%staff}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
