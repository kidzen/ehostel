<?php

use yii\db\Migration;

class m160818_050249_address extends Migration {
    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'address_for_user' => $this->smallInteger()->notNull()->defaultValue(0), //     1:staff ,      2:student 
            'user_id' => $this->integer()->notNull(),   //staff or student id
            'address' => $this->string(),
            'poskod' => $this->integer(),
            'state' => $this->string(),
            'country' => $this->string(),
            
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%address}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
