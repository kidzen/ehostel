<?php

use yii\db\Migration;

class m161031_064247_default_user extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }



        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'Admin1',
            'role_id' => 1,
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
            'email' => 'admin1@mail.com',
            'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
//            'last_log' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->insert('{{%staff}}', [
            'id' => 1,
            'user_id' => 1,
            'no_staff' => 'staff123',
            'no_ic' => '901231-01-1234',
            'no_tel' => '012-34567890',
            'email' => 'admin1@mail.com',
            'nama_staff' => 'Nama Staff 1',
            'dept_id' => 1,
            'staff_employer_id' => 1,
            'position_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);

    }

    public function down() {
//        $this->truncateTable('{{%staff}}');
//        $this->truncateTable('{{%user}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
