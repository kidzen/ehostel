<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = $modelParent->id;
$this->params['breadcrumbs'][] = ['label' => 'Hostels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info hostel-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Hostel : <?= Html::encode($modelParent->no_hostel) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Update', ['update', 'id' => $modelParent->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $modelParent->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <div>
            <?=
            DetailView::widget([
                'model' => $modelParent,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'no_hostel',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'status',
                        'vAlign' => 'middle',
                        'value' => $modelParent->status <= 0 ? 'Deleted' : 'Active',
                    ],
                    [
                        'attribute' => 'created_at',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'updated_at',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'created_by',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'updated_by',
                        'vAlign' => 'middle',
                    ],
                ],
            ])
            ?>
        </div>



        <?php foreach ($modelsChild as $modelChild) { ?>
            <div>
                <div class="box box-info bilik-view">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Bilik : <?= Html::encode($modelChild->no_bilik) ?></strong></h3>
                    </div>
                    <div class="box-body">
                        <p>
                            <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
                            <?= Html::a('Update', ['update', 'id' => $modelChild->id], ['class' => 'btn btn-primary']) ?>
                            <?=
                            Html::a('Delete', ['delete', 'id' => $modelChild->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>
                        </p>
                        <div>
                            <?=
                            DetailView::widget([
                                'model' => $modelChild,
                                'attributes' => [
                                    [
                                        'attribute' => 'id',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'hostel_id',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'no_bilik',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'vAlign' => 'middle',
                                        'value' => $modelChild->status <= 0 ? 'Deleted' : 'Active',
                                    ],
                                    [
                                        'attribute' => 'created_at',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'updated_at',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'created_by',
                                        'vAlign' => 'middle',
                                    ],
                                    [
                                        'attribute' => 'updated_by',
                                        'vAlign' => 'middle',
                                    ],
                                ],
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


