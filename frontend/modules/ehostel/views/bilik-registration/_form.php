<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bajadev\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Bilik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bilik-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="panel-body">
        <div class="row">

            <div class="col-sm-6">
                <?=
                $form->field($modelHostel, 'no_hostel')->widget(kartik\select2\Select2::className(), [
                    'data' => $hostelArray,
//                                        'theme' => 'default',
                    'options' => ['placeholder' => 'Select an inventory ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
        </div>

        <?php
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsBilik[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'full_name',
                'address_line1',
                'address_line2',
                'city',
                'state',
                'postal_code',
            ],
        ]);
        ?>

        <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsBilik as $i => $modelBilik): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left"> Bilik</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        if (!$modelBilik->isNewRecord) {
                            echo Html::activeHiddenInput($modelBilik, "[{$i}]id");
                        }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($modelBilik, "[{$i}]no_bilik")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
        <div class="form-group">
            <?= Html::submitButton($modelBilik->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
