<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Bilik';
$this->params['breadcrumbs'][] = ['label' => 'Biliks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="transactions-create">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!--<div class='row'>-->
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><i class="fa fa-upload"></i> <?= $this->title ?></h4></div>

        <div class="panel-body">
            <div class="row">
                <?php if (!$hostel->isNewRecord) { ?>
                    <div class="col-sm-4"> 
                        <?=
                        $form->field($hostel, 'hostel_id')->widget(kartik\select2\Select2::className(), [
                            'data' => $hostelArray,
//                            'theme' => 'default',
                            'options' => ['placeholder' => 'Select an inventory ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);
                        ?>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-4"> 
                        <?= $form->field($hostel, 'no_hostel')->widget(kartik\select2\Select2::className(), [
                            'data' => $hostelArray,
//                            'theme' => 'default',
                            'options' => ['placeholder' => 'Select an inventory ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]); ?>
                    </div>
                <?php } ?>
                <!--                                <div class="col-sm-4"> 
                <?php
                $approveStat = ['0' => "New", '1' => "Pending", '2' => "Approved",
                    '8' => "Rejected", '9' => "Disposed"];
                echo $form->field($hostel, 'APPROVED')
                        ->dropDownList(
                                $approveStat // Flat array ('id'=>'label')
                );
                ?>
                                                </div>-->
            </div>
            <!--////////////////////////////////estor items/////////////////////////////////-->

            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
//                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $bilikItems[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'ID',
                    'INVENTORY_ID',
                    'RQ_QUANTITY',
                ],
            ]);

//            var_dump($transaction->APPROVED);die();
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($bilikItems as $i => $bilikItem): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Items</h3>
                            <div class="pull-right">
                                <?php if (!$hostel->APPROVED) { ?>
                                    <button type = "button" class = "add-item btn btn-success btn-xs"><i class = "glyphicon glyphicon-plus"></i></button>
                                <?php } ?>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (!$bilikItem->isNewRecord) {
                                echo Html::activeHiddenInput($bilikItem, "[{$i}]ID");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <?= $form->field($model, "[{$i}]no_bilik")->textInput(['maxlength' => true]) ?>
                                </div>
                                <!--                                <div class="col-sm-4">
                                <?= $form->field($bilikItem, "[{$i}]CURRENT_BALANCE")->textInput(['maxlength' => true, 'value' => 0, 'readOnly' => true,]) ?>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
            <div class="form-group">
                <?= Html::submitButton($hostel->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $hostel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        </div>
        <!--</div>-->


    </div>
    <?php ActiveForm::end(); ?>

</div>
