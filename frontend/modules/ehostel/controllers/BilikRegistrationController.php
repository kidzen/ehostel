<?php

namespace frontend\modules\ehostel\controllers;

use Yii;
use common\components\Model;
use common\models\Hostel;
use common\models\Bilik;
use common\models\BilikSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;

/**
 * BilikController implements the CRUD actions for Bilik model.
 */
class BilikRegistrationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Bilik models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BilikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bilik model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new Bilik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $student = \common\models\StudentProfile::find()->asArray()->all();
        $bilik = \common\models\Bilik::find()->asArray()->all();
        $hostel = \common\models\Hostel::find()->asArray()->all();
        $studentArray = \yii\helpers\ArrayHelper::map($student, 'id', 'name');
        $bilikArray = \yii\helpers\ArrayHelper::map($bilik, 'id', 'no_bilik');
        $hostelArray = \yii\helpers\ArrayHelper::map($hostel, 'id', 'no_hostel');

        $model = new Hostel();
        $modelItems = [new Bilik()];

        $modelHostel = new Hostel;
        $modelsBilik = [new Bilik];
        if ($modelHostel->load(Yii::$app->request->post())) {
            $modelHostel = Hostel::findOne($modelHostel->id);
            $modelsBilik = Model::createMultiple(Bilik::classname());
            Model::loadMultiple($modelsBilik, Yii::$app->request->post());

            // ajax validation
//            if (Yii::$app->request->isAjax) {
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ArrayHelper::merge(
//                                ActiveForm::validateMultiple($modelsBilik), ActiveForm::validate($modelHostel)
//                );
//            }

            // validate all models
            $valid = $modelHostel->validate();
            $valid = Model::validateMultiple($modelsBilik) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelHostel->save(false)) {
                        foreach ($modelsBilik as $modelBilik) {
                            $modelBilik->hostel_id = $modelHostel->id;
                            if (!($flag = $modelBilik->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $modelHostel->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'hostelArray' => $hostelArray,
                    'modelHostel' => $modelHostel,
                    'modelsBilik' => (empty($modelsBilik)) ? [new Address] : $modelsBilik
        ]);
//        if ($model->load(Yii::$app->request->post())) {
//            $model = $this->findModel($model->ID);
//            $valid = $model->validate();
//
//            if ($model->save()) {
//                \Yii::$app->session->setFlash('success', [
//                    'type' => Growl::TYPE_SUCCESS,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-check-sign',
//                    'title' => ' Proses BERJAYA',
//                    'message' => ' Data berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            } else {
//                \Yii::$app->session->setFlash('error', [
//                    'type' => Growl::TYPE_DANGER,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-remove-sign',
//                    'title' => ' Proses GAGAL.',
//                    'message' => ' Data tidak berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            }
//            return $this->redirect(['view', 'id' => $model->id]);
//            //return $this->redirect(['index']);
//        } else {
//            return $this->render('create', [
//                        'model' => $model,
//                        'studentArray' => $studentArray,
//                        'bilikArray' => $bilikArray,
//                        'hostelArray' => $hostelArray,
//            ]);
//        }
    }

    /**
     * Updates an existing Bilik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-check-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bilik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->status = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-check-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->status = '10';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-check-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bilik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bilik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Bilik::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
