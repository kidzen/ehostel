<?php

namespace frontend\modules\ecuti;

/**
 * ecuti module definition class
 */
class ecuti extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\ecuti\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
