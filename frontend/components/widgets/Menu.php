<?php

namespace frontend\components\widgets;

class Menu extends \dmstr\widgets\Menu
{
     protected function renderItem($item)
    {
        if(isset($item['items'])) {
            $labelTemplate = '<a href="{url}">{label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';
            $linkTemplate = '<a href="{url}">{icon} {label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';
        }
        else {
            $labelTemplate = $this->labelTemplate;
            $linkTemplate = $this->linkTemplate;
        }

        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $linkTemplate);
            $replace = !empty($item['icon']) ? [
                '{url}' => Url::to($item['url']),
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => '<i class="' . $item['icon'] . '"></i> '
            ] : [
                '{url}' => Url::to($item['url']),
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => null,
            ];
            return strtr($template, $replace);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $labelTemplate);
            $replace = !empty($item['icon']) ? [
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => '<i class="' . $item['icon'] . '"></i> '
            ] : [
                '{label}' => '<span>'.$item['label'].'</span>',
            ];
            return strtr($template, $replace);
        }
    }
}



//namespace common\components;
//
//class AccessRule extends \yii\filters\AccessRule
//{
//    protected function matchRole($user)
//    {
//        if (empty($this->roles)) {
//            return true;
//        }
//        foreach ($this->roles as $role) {
//            if ($role === '?' && $user->getIsGuest()) {
//                return true;
//            } elseif ($role === '@' && !$user->getIsGuest()) {
//                return true;
//            } elseif (!$user->getIsGuest()) {
//                // user is not guest, let's check his role (or do something else)
////                if ($role === $user->identity->role) {
//                if ($role === $user->role) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

