<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StaffProfile */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Staff Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info staff-profile-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Staff Profile : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                [
            'attribute'=>'id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'user_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_staff',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_ic',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_tel',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'email',
            'format'=>'email',
            'vAlign' => 'middle',
            ],
            [
            'attribute'=>'nama_staff',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'dept_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'gambar',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'staff_employer_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'position_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'status',
                        'vAlign' => 'middle',
            'value' => $model->status <= 0 ? 'Deleted' : 'Active',
            ],
            [
            'attribute'=>'created_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'created_by',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_by',
                        'vAlign' => 'middle',
            ],
    ],
    ]) ?>

</div>
