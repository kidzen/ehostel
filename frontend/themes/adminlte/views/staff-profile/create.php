<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StaffProfile */

$this->title = 'Create Staff Profile';
$this->params['breadcrumbs'][] = ['label' => 'Staff Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-profile-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
