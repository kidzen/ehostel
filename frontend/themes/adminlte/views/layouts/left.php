<?php

use yii\helpers\Url;
?>

<aside class="main-sidebar">

    <section class="sidebar">
        <!--    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 409px;">
                <section class="sidebar" style="height: 409px; overflow: hidden; width: auto;">-->

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <!--            <div class="pull-left image">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                        </div>
                        <div class="pull-left info">
                            <p><?= $username ?></p>
            <?php if (Yii::$app->user->id) { ?>
                                                                                                                                                                    <a href="#"><i class="fa fa-circle text-success"></i> <?= $activeBool ?></a>
            <?php } else { ?>
                                                                                                                                                                    <a href="#"><i class="fa fa-circle text-danger"></i> <?= $activeBool ?></a>
            <?php } ?>
                            
                        </div>-->
            <?php if (Yii::$app->user->id) { ?>
                <div class="user-panel center-block">
                    <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-bordered img-responsive img-circle center-block" alt="user_pic"/>

                    <h3 class="text-center"><?= $username ?></h3>
                    <h5 class="text-center"><?= $roles ?></h5>
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle img-bordered img-responsive"  alt="user_pic"/>-->
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle center-block" style="width: 160px" alt="user_pic"/>-->

                </div>
            <?php } else { ?>
                <div class="user-panel center-block">
                    <img src="<?= $directoryAsset ?>/img/sistem.png" class="img-bordered img-responsive img-circle center-block" alt="sistem_logo"/>

                    <h3 class="text-center"><?= $username ?></h3>
                    <h5 class="text-center"><?= $roles ?></h5>
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle img-bordered img-responsive"  alt="user_pic"/>-->
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle center-block" style="width: 160px" alt="user_pic"/>-->

                </div>
            <?php } ?>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                      <span class="input-group-btn">
                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>-->
        <!-- /.search form -->
        <div style="overflow: true">
            <?=
            dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu delay'],
                        'items' => [
//                            ['label' => '', 'options' => ['class' => 'header']],
                            ['label' => 'Dashboard', 'icon' => 'fa fa-home', 'url' => ['site/index']],
//                            ['label' => 'e-Cuti', 'icon' => 'fa fa-home', 'url' => Url::home().'ecuti'],
//                            ['label' => 'e-Hostel', 'icon' => 'fa fa-home', 'url' => Url::home().'ehostel'],
//                        ['label' => 'Inventori Stor', 'icon' => 'fa fa-home', 'url' => ['entry/index']],
//                        ['label' => 'Kad Inventori', 'icon' => 'fa fa-home', 'url' => ['estor-inventories/index']],
//                        ['label' => 'Senarai Stok', 'icon' => 'fa fa-home', 'url' => ['estor-items/index']],
//                        ['label' => 'Permohonan Stok', 'icon' => 'fa fa-home', 'url' => ['request/create']],
//                        ['label' => 'Senarai Permohonan Stok', 'icon' => 'fa fa-home', 'url' => ['request/index']],
//                        ['label' => 'Approval', 'icon' => 'fa fa-home', 'url' => ['approval2/index']],
//                        ['label' => 'Order', 'icon' => 'fa fa-home', 'url' => ['stocks/index']],
//                        ['label' => 'Order Item', 'icon' => 'fa fa-home', 'url' => ['stock-items/index']],
                            [
                                'label' => 'e-Hostel',
                                'icon' => 'fa fa-building',
                                'style' => 'color:blue;',
                                //'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Management', 'icon' => 'fa fa-gear', 'url' => '#',
                                        'items' => [
                                            ['label' => 'Hostel Management', 'icon' => 'fa fa-file-o', 'url' => ['hostel-management/create'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                        ],
                                    ],
                                    ['label' => 'Student Room Registration', 'icon' => 'fa fa-file-code-o', 'url' => ['bilik-assignment/create'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Student Room List', 'icon' => 'fa fa-file-code-o', 'url' => ['bilik-assignment/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                ],
                            ],
                            [
                                'label' => 'e-Cuti',
                                'icon' => 'fa fa-calendar',
                                'style' => 'color:blue;',
                                //'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Management', 'icon' => 'fa fa-gear', 'url' => '#',
                                        'items' => [
                                            ['label' => 'Position', 'icon' => 'fa fa-file-code-o', 'url' => ['position/index'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                            ['label' => 'Department', 'icon' => 'fa fa-file-code-o', 'url' => ['department/index'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                        ],
                                    ],
//                                    ['label' => 'Bilik Assignment', 'icon' => 'fa fa-file-code-o', 'url' => ['ehostel/bilik-assignment'],
//                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                ],
                            ],
                            [
                                'label' => 'Pentadbiran Sistem',
                                'icon' => 'fa fa-gear',
                                'style' => 'color:blue;',
                                //'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Roles', 'icon' => 'fa fa-file-code-o', 'url' => ['role/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Student', 'icon' => 'fa fa-file-code-o', 'url' => ['student-profile/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Users', 'icon' => 'fa fa-dashboard', 'url' => ['user-profile/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'My Profile', 'icon' => 'fa fa-user', 'url' => ['user-profile/view', 'id' => Yii::$app->user->id],],
                                ],
                            ],
                            ['label' => 'Login', 'icon' => 'fa fa-sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            [
                                'label' => 'Developer tools',
                                'icon' => 'fa fa-share',
                                // 'visible' => Yii::$app->user->isAdmin,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                                    [
                                        'label' => 'Database',
                                        'icon' => 'fa fa-share',
                                        //       'visible' => Yii::$app->user->isAdmin,
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Stock', 'icon' => 'fa fa-cart-plus', 'url' => ['/stocks/index']],
                                            ['label' => 'Stock Items', 'icon' => 'fa fa-cubes', 'url' => ['/stock-items/index']],
                                            ['label' => 'Estor Items', 'icon' => 'fa fa-cubes', 'url' => ['/estor-items/index']],
                                        ],
                                    ],
//                                [
//                                    'label' => 'Level One',
//                                    'icon' => 'fa fa-circle-o',
//                                    'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                        [
//                                            'label' => 'Level Two',
//                                            'icon' => 'fa fa-circle-o',
//                                            'url' => '#',
//                                            'items' => [
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                            ],
//                                        ],
//                                    ],
//                                ],
                                ],
                            ],
                        ],
                    ]
            )
            ?>
        </div>
    </section>


    <!--            <div class="slimScrollBar" style="width: 3px; position: absolute; top: 31px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 169.656px; background: rgba(0, 0, 0, 0.2);">
                </div>
                <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);">
                </div>
                </div>-->
</aside>
