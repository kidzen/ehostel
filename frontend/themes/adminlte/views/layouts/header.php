<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$counter1 = 1;
$datas1 = ['data10','data11'];

$counter2 = 2;
$datas2 = ['data20','data21'];

$counter3 = 3;
$datas3 = ['data30','data31'];

$counter4 = 4;
$datas4 = ['data40','data41'];

$class1 = '';
?>

<header class="main-header">
    <?PHP // ECHO Html::a('<span class="logo-mini">  <img src="' . $directoryAsset . '/img/favicon-32x32.png"  alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> </span><span class="logo-lg"><img src="' . $directoryAsset . '/img/favicon-32x32.png" style="padding-right: 10px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai">' . Yii::$app->name . '</></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <?= Html::a('<span class="logo-mini">  <img src="' . $directoryAsset . '/img/favicon-32x32.png"  alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> </span><span class="logo-lg"><img src="' . $directoryAsset . '/img/favicon-32x32.png" style="padding-right: 10px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai">MPSP</></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-archive"></i>
                        <span class="label label-success"><?= $counter1 ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= $counter1 ?> messages</li>
                        <li>
                            <!--inner menu: contains the actual data--> 
                            <ul class="menu">
                        <?php foreach ($datas1 as $data1) { ?>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle"
                                                 alt="User Image"/>
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                        <?php } ?>
                                <!--end message--> 
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>
                <!--//////-->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-legal"></i>
                        <span class="label label-warning"><?= $counter1 ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= $counter1 ?> notifications</li>
                        <li>
                            <!--inner menu: contains the actual data--> 
                            <ul class="menu">
                        <?php // foreach ($datas2 as $data1) { ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> <?= $counter1 ?> new members joined today
                                    </a>
                                </li>
                        <?php // } ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may
                                        not fit into the page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> <?= $counter1 ?> new members joined
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> <?= $counter1 ?> sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!--////////////-->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-archive"></i>
                        <span class="label label-warning"><?= $counter1 ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= $counter1 ?> notifications</li>
                        <li>
                            <!--inner menu: contains the actual data--> 
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> <?= $counter1 ?> new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may
                                        not fit into the page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> <?= $counter1 ?> new members joined
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> <?= $counter1 ?> sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!--////////////-->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-stack-overflow"></i>
                        <span class="label label-warning"><?= $counter1 ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= $counter1 ?> notifications</li>
                        <li>
                            <!--inner menu: contains the actual data--> 
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> <?= $counter1 ?> new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may
                                        not fit into the page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> <?= $counter1 ?> new members joined
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> <?= $counter1 ?> sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!--////////////-->
                <!--Tasks: style can be found in dropdown.less--> 
<!--                <li class="dropdown tasks-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="createButton">
                                            <i class="fa fa-flag-o"></i>
                                            <span class="label label-danger">9</span>
                                        </a>
                    <?php // echo Html::a('<i class="fa fa-flag-o"></i>', ['value' => Url::to('index.php?r=site/about'), 'type' => 'button', 'title' => Yii::t('app', 'Info'), 'class' => 'dropdown-toggle', 'id' => 'infoButton']) ?>
                    <?= Html::button(
                            '<i class="fa fa-flag-o"></i>', ['value' => Url::to('index.php?r=site/about'), 'type' => 'button', 'title' => Yii::t('app', 'Info'), 'class' => 'dropdown-toggle', 'id' => 'infoButton'])
                    ?>
                    <?php // echo Html::a(
//                            '<i class="fa fa-flag-o"></i>', Url::to('index.php?r=site/about'), ['class' => 'dropdown-toggle','id' => 'infoButton'])
                    ?>
                    

                </li>-->
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= $username ?> - <?= $roles ?>
<!--                                <small>Member since Nov. 2012</small>-->
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--                        <li class="user-body">
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Followers</a>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Sales</a>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Friends</a>
                                                    </div>
                                                </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <?php if (isset(Yii::$app->user->id)) { ?>
                                <div class="pull-left">
                                    <?=
                                    Html::a(
                                            'Profile', ['people/view', 'id' => Yii::$app->user->id], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>

                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                            'Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            <?php } else { ?>
                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                            'Sign In', ['/site/login'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            <?php } ?>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <!--                <li>
                                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                </li>-->
            </ul>
        </div>
    </nav>
</header>
<?php
$js = <<<JS

    $('#gs1').change(function(){
            var gsId = $(this).val();
            alert(gsId);
       });
                                
//    $('#createButton').click(function(){
////            var gsId = $(this).val();
////            alert('hai');
//       });

   $('#infoButton').click(function(){
            $('#info').modal('show')
                .find('#infoContent')
                .load($(this).attr('value'));
            });

//    function create(){
//            alert('hai');
//            }
                                
//    $('#createButton').click(function(){
//            var gsId = $(this).val();
//            alert(gsId);
//       });
JS;
$this->registerJs($js);
