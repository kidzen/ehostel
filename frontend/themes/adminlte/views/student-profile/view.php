<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StudentProfile */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Student Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info student-profile-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Student Profile : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                [
            'attribute'=>'id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'nama',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_matrik',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_ic',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_tel',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'email',
            'format'=>'email',
            'vAlign' => 'middle',
            ],
            [
            'attribute'=>'status',
                        'vAlign' => 'middle',
            'value' => $model->status <= 0 ? 'Deleted' : 'Active',
            ],
            [
            'attribute'=>'created_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'created_by',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_by',
                        'vAlign' => 'middle',
            ],
    ],
    ]) ?>

</div>
