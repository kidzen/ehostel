<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BilikAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bilik-assignment-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
                        <div class="col-md-4">
                <?= $form->field($model, 'student_id')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'bilik_id')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'date_register')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'valid_until')->textInput() ?>
            </div>

        </div>
        <div>
            <?= Html::a('Cancel' , Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
