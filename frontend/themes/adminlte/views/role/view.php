<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info role-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Role : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                [
            'attribute'=>'id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'name',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'description',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'status',
                        'vAlign' => 'middle',
            'value' => $model->status <= 0 ? 'Deleted' : 'Active',
            ],
            [
            'attribute'=>'created_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_at',
                        'vAlign' => 'middle',
            ],
    ],
    ]) ?>

</div>
